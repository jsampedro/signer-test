import React from 'react';
import Image from './Image';

let DocumentPage = React.createClass({
    getInitialState: function () {
        return {signatureImage: null};
    },
    handleClick: function (e) {
        this.props.onClick(e, this);
    },
    positionImage: function (image) {
        this.setState({signatureImage: image});
    },
    render: function () {

        let signature = this.state.signatureImage ?
            <Image src={this.state.signatureImage}/> : null;
        return <div id={"page-" + this.props.page.number} style={ {
            backgroundColor: 'grey',

        } }>
            <img className="img-responsive"
                 src={this.props.page.src}
                 alt="document page"
                 onClick={this.handleClick}
                 style={ {display: 'inline-block'} }

            />

            {signature}
        </div>;
    }
});

export default DocumentPage;
