import React from 'react';
import Rnd from 'react-rnd';

const style = {
    border: 'solid 3px #fff',
    color: '#fff',
    display: 'inline-block'
};

let Image = React.createClass({

    accept: function () {
        this.props.onConfirmPosition(this.findPos(this.refs.image));
    },
    findPos: function (element) {
        let bodyRect = document.body.getBoundingClientRect(),
            elemRect = element.getBoundingClientRect();

        return {
            x: elemRect.left - bodyRect.left,
            y: elemRect.top - bodyRect.top
        };

    },
    render: function () {

        return <Rnd
            ref={c => {
                this.rnd = c;
            }}
            style={style}

            lockAspectRatio={false}
            bounds={'parent'}
        >
            <div className="panel panel-info" style={{
                background: 'none',
                width: '100%',

            }}>
                <div className="handle panel-heading">
                    <b style={{cursor: 'move'}}>
                        <i className="fa fa-arrows"/> Mueva la firma a la posición que desee.
                    </b>
                </div>
                <div className="panel-body">
                    <img src={this.props.src} ref="image"/>

                </div>
                <div className="panel-footer">
                    <button className="btn btn-success" onClick={this.accept}> Aceptar</button>
                </div>

            </div>
        </Rnd>

            ;
    }
});

export default Image;
