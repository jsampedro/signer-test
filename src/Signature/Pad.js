import React from 'react';
import "./style/pad.css";
import SignaturePad from "signature_pad";

let Pad = React.createClass({
    componentDidMount: function () {
        window.onresize = this.resizeCanvas;
        this.canvas = document.getElementById('canvas');
        this.resizeCanvas();

        this.signaturePad = new SignaturePad(this.canvas);
    },

    resizeCanvas: function () {
        let ratio = Math.max(window.devicePixelRatio || 1, 1);
        this.canvas.width = this.canvas.offsetWidth * ratio;
        this.canvas.height = this.canvas.offsetHeight * ratio;
        this.canvas.getContext("2d").scale(ratio, ratio);
    },
    sign: function () {
        this.props.onSigned(
            this.cropImageFromCanvas(this.canvas.getContext("2d"), this.canvas)
        );
    },
    cropImageFromCanvas: function (ctx, canvas) {

        let w = canvas.width,
            h = canvas.height,
            pix = {x: [], y: []},
            imageData = ctx.getImageData(0, 0, canvas.width, canvas.height),
            x, y, index;

        for (y = 0; y < h; y++) {
            for (x = 0; x < w; x++) {
                index = (y * w + x) * 4;
                if (imageData.data[index + 3] > 0) {
                    pix.x.push(x);
                    pix.y.push(y);
                }
            }
        }
        pix.x.sort(function (a, b) {
            return a - b
        });
        pix.y.sort(function (a, b) {
            return a - b
        });
        let n = pix.x.length - 1;

        w = pix.x[n] - pix.x[0];
        h = pix.y[n] - pix.y[0];
        let cut = ctx.getImageData(pix.x[0], pix.y[0], w, h);

        canvas.width = w;
        canvas.height = h;
        ctx.putImageData(cut, 0, 0);

        return canvas.toDataURL();
    },

    cancel: function () {
        this.props.onCancel();
    },
    render: function () {

        return <div id="signature-pad" className="m-signature-pad">
            <div className="m-signature-pad--body">
                <canvas id="canvas"/>
            </div>
            <div className="m-signature-pad--footer">
                <div className="description">Sign above</div>
                <button type="button" className="button clear" onClick={this.cancel}>Cerrar</button>
                <button type="button" className="button save" onClick={this.sign}>Aceptar</button>
            </div>
        </div>;
    }
});

export default Pad;
