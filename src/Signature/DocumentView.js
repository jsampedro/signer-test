import React from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import _ from 'lodash';
import DocumentPage from './DocumentPage';

let DocumentView = React.createClass({
    getInitialState: function () {
        return {visiblePages: [], hasMorePages: true};
    },

    onPositionSelected: function (position, page) {
        this.props.onPositionSelected(position, page);
    },

    loadPage: function (page) {
        this.setState({
            visiblePages: _.slice(this.props.pages, 0, page),
            hasMorePages: this.props.pages.length > page
        });
    },
    handleOnPageClick: function (e, page) {
        this.props.onPageClick(e, page);
    },
    render: function () {
        let pages = this.state.visiblePages.map(function (page) {
            return <DocumentPage page={page} key={page.number} onClick={this.handleOnPageClick}/>
        }, this);


        return <InfiniteScroll
            loadMore={this.loadPage}
            hasMore={this.state.hasMorePages}
            loader={<div className="loader">Loading ...</div>}>

            {pages}

        </InfiniteScroll>
    }
});

export default DocumentView;
