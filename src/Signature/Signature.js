import React from 'react';
import Modal from "react-modal";
import Image from "./Image";
import Pad from "./Pad";
import "./style/signature.css";
import DocumentView from "./DocumentView"

let Signature = React.createClass({
    getInitialState: function () {
        return {
            capturedSignature: null,
            showingPad: false,
            page: null,
            imagePosition: null
        };
    },
    handleSigned: function () {
        this.props.onCompleteSignature(this.props.params.signatureId);

    },
    handlePageClick: function (e, page) {
        if (!this.state.showingPad && !this.state.capturedSignature) {
            this.setState({
                imagePosition: {
                    x: e.nativeEvent.pageX,
                    y: e.nativeEvent.pageY
                },
                page: page
            });

            this.openPad();
        }
    },
    openPad: function () {
        this.setState({
            showingPad: true
        });
    },
    onSigned: function (capturedSignature) {
        this.setState({showingPad: false, capturedSignature: capturedSignature});
        this.state.page.positionImage(capturedSignature);
    },
    cancelSignature: function () {
        this.setState({showingPad: false, capturedSignature: null});
    },
    confirmSignature: function (position) {

        let {x, y} = position;

        let pdfWidth = 210;
        let pdfHeight = 297;
        let convertedPosition = {
            x: x * pdfWidth / this.state.page.width,
            y: y * pdfHeight / this.state.page.height
        };

        this.props.onConfirmedSignature(this.props.params.signatureId, convertedPosition, this.state.page.number, this.state.capturedSignature);

    },

    render: function () {

        let signatureImage = this.state.capturedSignature ?
            <Image src={this.state.capturedSignature}
                   position={this.state.imagePosition}
                   onConfirmPosition={this.confirmSignature}
                   page={this.state.page}
            /> : null;

        return <div>
            <Modal
                isOpen={this.state.showingPad}
                className="modalContent"
                overlayClassName="modalOverlay"

                contentLabel="Modal"
            >
                <Pad onSigned={this.onSigned}
                     onCancel={this.cancelSignature}/>
            </Modal>

            {/*<nav className="navbar navbar-default navbar-fixed-top">*/}
            {/*<div className="container">*/} {/*<nav className="navbar navbar-default navbar-fixed-top">*/}
            {/*<div className="container">*/}
            {/*<h3>*/}
            {/*Pulse en la posicion en la que desea firmar*/}
            {/*</h3>*/}
            {/*</div>*/}
            {/*</nav>*/}
            {/*<h3>*/}
            {/*Pulse en la posicion en la que desea firmar*/}
            {/*</h3>*/}
            {/*</div>*/}
            {/*</nav>*/}


            <DocumentView pages={this.props.pages}
                          onPageClick={this.handlePageClick}
                          signatureImage={signatureImage}
            />


        </div>;
    }
});

export default Signature;
