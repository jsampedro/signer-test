import React from 'react';
import Signature from './Signature/Signature'
import ApiClient from './ApiClient';
import Alert from "react-s-alert";

let App = React.createClass({

        onCompleteSignature: function (signatureId) {
            ApiClient.completeSignature(signatureId);

            Alert.success('<b>Documento firmado correctamente.</b>', {
                timeout: 5000,
                html: true
            });
        },
        confirmSignature: function (id, position, pageNumber, image) {
            ApiClient.postSignature(id, position, pageNumber, image).then(
                function () {
                    Alert.success('<b>Documento firmado correctamente.</b>', {
                        timeout: 5000,
                        html: true
                    });
                }
            );
        },

        render: function () {

            let pages = [
                {
                    number: 1,
                    dimension: {width: 400, height: 500},
                    src: './img/test.png'
                },
                {
                    number: 2,
                    dimension: {width: 600, height: 800},
                    src: './img/test2.png'
                }
            ];
            return <Signature
                onCompleteSignature={this.onCompleteSignature}
                onConfirmedSignature={this.confirmSignature}
                pages={pages}
            />;
        }
    })
;

export default App;
